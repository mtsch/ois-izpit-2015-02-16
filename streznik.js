var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	var q = req.query;
	uporabnikiSpomin.push({
		davcnaStevilka : q.ds,
		ime : q.ime,
		priimek : q.priimek,
		naslov : q.ulica,
		hisnaStevilka : q.hisnaStevilka,
		postnaStevilka : q.postnaStevilka,
		kraj : q.kraj,
		drzava : q.drzava,
		poklic : q.poklic,
		telefonskaStevilka : q.telefonskaStevilka
	});
	res.redirect("/");
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	var dstev = req.query.id;
	if (dstev === undefined) {
		alert("Napačna zahteva!");
	}
	for (var i=0; i<uporabnikiSpomin.length; i++) {
		var tu = uporabnikiSpomin[i];
		if (tu.davcnaStevilka == dstev) {
			uporabnikiSpomin.splice(i,1);
			res.redirect("/");
			return;
		}
	}
	res.send("Oseba z davčno številko "+dstev+" ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
	
	//res.send('Potrebno je implementirati brisanje oseb!');
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];